package com.example.ecommerce.repository;

import com.example.ecommerce.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BrandRepo  extends JpaRepository<Brand, Long> {
}
