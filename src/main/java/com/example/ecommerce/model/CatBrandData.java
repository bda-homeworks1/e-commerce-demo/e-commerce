package com.example.ecommerce.model;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;



@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CatBrandData {
    Long id;

    Long categoryId;
    Long brandId;

}
