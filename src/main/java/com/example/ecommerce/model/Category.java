package com.example.ecommerce.model;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Category {

    Long id;
    String name;
    String desc;



}
