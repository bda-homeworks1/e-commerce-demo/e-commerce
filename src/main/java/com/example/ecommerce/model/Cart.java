package com.example.ecommerce.model;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Cart {

    /*Category – id, name, desc
Brand – id, name, desc
Product – İd, name, desc, amount, remainCount, productDetails
User – id, name, surname, birthdate, email, address
Cart – id, product, count, total amount*/

    Long id;
    Long categoryId;
    Long brandId;
    Long productId;
    String productName;
}
