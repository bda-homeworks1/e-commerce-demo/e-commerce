package com.example.ecommerce.model;


import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product {
    /*
    *
    * Category – id, name, desc
Brand – id, name, desc
Product – İd, name, desc, amount, remainCount, productDetails
User – id, name, surname, birthdate, email, address
Cart – id, product, count, total amount
    *
    *
    *
    * */

    Long id;
    Long catBrandId;
    String name;
    String desc;
    BigDecimal amount;
    Long remainCount;
    String productDetails;


}
