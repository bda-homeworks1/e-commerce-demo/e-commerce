package com.example.ecommerce.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cart")

public class Cart {
    @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    Category categoryId;

    @ManyToOne
    Brand brandId;

    Long productId;


    String productName;
}
